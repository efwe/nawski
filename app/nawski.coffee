schedule = require('node-schedule')
express = require('express')
winston = require('winston')
http = require('http')
path = require('path')
moment = require('moment')
Snapshot = require( __dirname + '/jobs/snapshot')
PicturePool = require(__dirname + '/picture_pool')

class Nawski
  env = process.env.NODE_ENV || 'development'
  config = require(__dirname + '/'+env+'-config')
  logger = new (winston.Logger)({
    transports: [
      new (winston.transports.Console)({ level: config.winston.level, colorize: true, timestamp: true })
    ]
  })

  constructor: () ->
    logger.info "starting nawski"
    @picturePool = new PicturePool()
    snapshot = new Snapshot()
    job = schedule.scheduleJob '15 8-18 * * *', snapshot

  boot: () ->
    app = express()
    app.set('port', config.express.port)
    app.set('views', __dirname + '/views')
    app.set('view engine', 'jade')
    app.use(express.static(path.join(__dirname, 'public')))
    app.use(express.static(config.picpool.base_path))

    # our picture pages
    app.get '/:date', (req, res) =>
      date = moment(req.params.date)
      pics = @picturePool.picsForDate(date)
      vars = {
        pics: pics,
        this_date: date.format('YYYY-MM-DD')
        next_date: moment(date).add(1,'d').format('YYYY-MM-DD')
        prev_date: moment(date).subtract(1,'d').format('YYYY-MM-DD')
      }
      res.render 'index', vars

    # deliver the index.html
    app.get '/', (req, res) =>
      res.redirect("/#{moment().format('YYYY-MM-DD')}");
    server = http.createServer(app).listen app.get('port'), () ->
      logger.info "nawski booted (#{env}) - listening on port #{app.get('port')}"

module.exports=Nawski
