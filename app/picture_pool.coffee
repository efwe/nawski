winston = require('winston')
moment = require('moment')
fs = require('fs')
lwip = require('lwip')

class PicturePool
  env = process.env.NODE_ENV || 'development'
  config = require(__dirname + '/'+env+'-config')
  logger = new (winston.Logger)({
    transports: [
      new (winston.transports.Console)({ level: config.winston.level, colorize: true, timestamp: true })
    ]
  })

  constructor: () ->
    logger.info "booting picture pool in #{config.picpool.base_path}"


  startsWith :(str,prefix) ->
    return str.indexOf(prefix) == 0

  picsForDate: (date) ->
    result = []
    pics = fs.readdirSync(config.picpool.base_path)
    for pic in pics
      if  @startsWith(pic,date.format('YYYY-MM-DD'))
        result.push pic
    return result

  @ThumbFor: (req,res) ->
    pic = req.param('id')
    logger.info "rendering thumb for #{pic}"
    lwip.open config.picpool.base_path+'/'+pic, (err, image) ->
      if err
        logger.error "unable to open pic #{pic}"
        res.status(404).send("*boink* *#{err}*")
      else
        # we assume full size raspi pics (2592x1944)
        image.resize 400, 300, (err, image) ->
          if err
            res.status(500).send("no thumb for you: #{err}")
          else
            image.toBuffer 'jpg', (err, buffer) ->
              if err
                res.status(500).send("buffer is b0rked #{err}")
              else
                res.set('Content-Type', 'image/jpeg')
                res.send buffer




module.exports=PicturePool
