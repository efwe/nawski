winston = require('winston')
moment = require('moment')
lwip = require('lwip')
RaspiCam = require('raspicam')

class Snapshot
  env = process.env.NODE_ENV || 'development'
  config = require(__dirname + '/../'+env+'-config')
  logger = new (winston.Logger)({
    transports: [
      new (winston.transports.Console)({ level: config.winston.level, colorize: true, timestamp: true })
    ]
  })

  thumbFor: (filename) ->
    logger.info "rendering thumb for #{filename}"
    lwip.open config.picpool.base_path+'/'+filename, (err, image) ->
      if err
        logger.error "unable to open pic #{pic}: #{err}"
      else
        # we assume full size raspi pics (2592x1944)
        logger.debug "starting resize"
        image.resize 400, 300, (err, image) ->
          if err
            logger.error "unable to open pic #{pic}: #{err}"
          else
            logger.debug "writing to buffer"
            image.toBuffer 'jpg', (err, buffer) ->
              if err
                logger.error "buffer is b0rked #{err}"
              else
                outfile = config.picpool.base_path+'/tmb_'+filename 
                logger.debug "writing to file: #{outfile}"
                image.writeFile outfile, (err) ->
                  if err
                    logger.error "unable to write to #{outfile}: #{err}"
                  else
                    logger.info "created thumb"


  endsWith :(str, suffix) ->
    return str.indexOf(suffix, str.length - suffix.length) != -1

  startsWith :(str,prefix) ->
    return str.indexOf(prefix) == 0 

  execute: () ->
    logger.info "taking snapshot"
    camera = new RaspiCam({
    mode: "photo",
    output: "/data/pics/#{moment().format('YYYY-MM-DD-HHmm')}.jpg",
    encoding: "jpg",
    width: 2592,
    height: 1944,
    })
    outfile = ""
    camera.on "started", ( err, timestamp )->
      logger.info "photo started at #{timestamp}"
    camera.on "read", ( err, timestamp, filename ) =>
      logger.info "photo image captured with filename: #{filename}"
      # let's take a thumbnail after all
      if @endsWith(filename, 'jpg')
        outfile = filename

    camera.on "exit", ( timestamp )=>
      logger.info "snapshot taken - creating thumb"
      @thumbFor outfile

    camera.on "stop", ( err, timestamp ) ->
      logger.info "photo child process has been stopped at #{timestamp}"

    camera.start()

module.exports=Snapshot
